#include "util-linux-automotive.h"

#include <string>
#include <sys/stat.h>
#include <unistd.h>

#define autoclose __attribute__ ((cleanup (cleanup_close)))

static inline void
cleanup_close (int *fd)
{
  if (*fd >= 0)
    close (*fd);
}

static int
create_partlabel_symlink (const char *devname, const char *partlabel)
{
  std::string partlabel_escaped = "/dev/disk/by-partlabel/";
  for (size_t i = 0; partlabel[i]; ++i)
    if (partlabel[i] == ' ')
      partlabel_escaped += "\\x20";
    else
      partlabel_escaped += partlabel[i];

  const std::string devname_final = "../../" + std::string (basename (devname));
  debug ("symlink (\"%s\", \"%s\")", devname_final.c_str (), partlabel_escaped.c_str ());
  if (symlink (devname_final.c_str (), partlabel_escaped.c_str ()) < 0)
    {
      perrorf ("symlink (\"%s\", \"%s\")", devname_final.c_str (), partlabel_escaped.c_str ());
      return 4;
    }

  return 0;
}

static int
create_partuuid_symlink (const char *devname, const char *partuuid)
{
  std::string partuuid_full = "/dev/disk/by-partuuid/" + std::string (partuuid);
  const std::string devname_final = "../../" + std::string (basename (devname));
  debug ("symlink (\"%s\", \"%s\")", devname_final.c_str (), partuuid_full.c_str ());
  if (symlink (devname_final.c_str (), partuuid_full.c_str ()) < 0)
    {
      perrorf ("symlink (\"%s\", \"%s\")", devname_final.c_str (), partuuid_full.c_str ());
      return 5;
    }

  return 0;
}

static int
each_block_device (const char *devname)
{
  const char *val;
  autoblkid_free_probe blkid_probe pr = blkid_new_probe ();
  if (!pr)
    return -1;

  blkid_probe_enable_superblocks (pr, 0);
  blkid_probe_enable_partitions (pr, 1);
  blkid_probe_set_partitions_flags (pr, BLKID_PARTS_ENTRY_DETAILS);
  autoclose int fd = open (devname, O_RDONLY | O_CLOEXEC);
  if (fd < 0)
    return errno;

  if (blkid_probe_set_device (pr, fd, 0, 0))
    return -1;

  const int rc = blkid_do_safeprobe (pr);
  if (rc)
    return rc;

  if (!blkid_probe_lookup_value (pr, "PART_ENTRY_NAME", &val, nullptr))
    create_partlabel_symlink (devname, val);

  if (!blkid_probe_lookup_value (pr, "PART_ENTRY_UUID", &val, nullptr))
    create_partuuid_symlink (devname, val);

  return 0;
}

int
main (int argc, char *[])
{
  if (argc > 1)
    enable_debug = true;

  autoglobfree glob_t glob_result;
  for (int i = 0; i < num_patterns; ++i)
    glob (patterns[i], i ? GLOB_APPEND : 0, nullptr, &glob_result);

  mkdir ("/dev/disk", 0755);
  mkdir ("/dev/disk/by-partlabel", 0755);
  mkdir ("/dev/disk/by-partuuid", 0755);
  for (size_t i = 0; i < glob_result.gl_pathc; ++i)
    each_block_device (glob_result.gl_pathv[i]);

  return 0;
}
