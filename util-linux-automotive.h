#include <blkid/blkid.h>
#include <errno.h>
#include <fcntl.h>
#include <glob.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef __cplusplus
#define UNSAFE_START \
  _Pragma ("GCC diagnostic push") _Pragma ("GCC diagnostic ignored \"-Wsign-conversion\"") \
      _Pragma ("GCC diagnostic ignored \"-Wold-style-cast\"")
#else
#define UNSAFE_START \
  _Pragma ("GCC diagnostic push") _Pragma ("GCC diagnostic ignored \"-Wsign-conversion\"") \
      _Pragma ("GCC diagnostic ignored \"-Wincompatible-pointer-types\"")
#endif

#define UNSAFE_END _Pragma ("GCC diagnostic pop")

#define autofree __attribute__ ((cleanup (cleanup_free)))
#define autoblkid_free_probe __attribute__ ((cleanup (cleanup_free_probe)))
#define autoglobfree __attribute__ ((cleanup (cleanup_globfree)))

// Define the patterns
static const char *patterns[]
    = { "/dev/loop*",  "/dev/mmcblk*[0-9]", "/dev/msblk*[0-9]", "/dev/mspblk*[0-9]", "/dev/nvme*",
        "/dev/sd*",    "/dev/sr*",          "/dev/vd*",         "/dev/xvd*",         "/dev/bcache*",
        "/dev/cciss*", "/dev/dasd*",        "/dev/ubd*",        "/dev/ubi*",         "/dev/scm*",
        "/dev/pmem*",  "/dev/nbd*",         "/dev/zd*" };

static const int num_patterns = sizeof (patterns) / sizeof (patterns[0]);

static bool enable_debug = false;

__attribute__ ((__format__ (printf, 1, 2))) static void
debug (const char *format, ...)
{
  if (enable_debug)
    {
      va_list args;
      va_start (args, format);
      vprintf (format, args);
      printf ("\n");
      va_end (args);
    }
}

__attribute__ ((__format__ (printf, 1, 2))) static void
perrorf (const char *format, ...)
{
  va_list args;
  va_start (args, format);
  vfprintf (stderr, format, args);
  fprintf (stderr, ": %s\n", strerror (errno));
  va_end (args);
}

static inline void
cleanup_free (void *p)
{
  UNSAFE_START
  free (*(void **)p);
  UNSAFE_END
}

static inline void
cleanup_free_probe (blkid_probe *p)
{
  blkid_free_probe (*p);
}

static inline void
cleanup_globfree (glob_t *glob_result)
{
  globfree (glob_result);
}
