#!/bin/bash

set -e

for i in mount-sysroot.c dev-disk-populate.cpp; do
clang-format --style=file -i $i
WARN_FLAGS="-Wall -Winvalid-pch -Wextra -Wpedantic -Wcast-qual -Wconversion -Wfloat-equal -Wformat=2 -Winline -Wmissing-declarations -Wredundant-decls -Wshadow -Wundef -Wuninitialized -Wwrite-strings -Wdisabled-optimization -Wpacked -Wpadded -Wmultichar -Wswitch-default -Wswitch-enum -Wunused-macros -Wmissing-include-dirs -Wunsafe-loop-optimizations -Wstack-protector -Wstrict-overflow=5 -Warray-bounds=2 -Wlogical-op -Wstrict-aliasing=3 -Wvla -Wdouble-promotion -Wsuggest-attribute=const -Wsuggest-attribute=noreturn -Wsuggest-attribute=pure -Wtrampolines -Wvector-operation-performance -Wsuggest-attribute=format -Wdate-time -Wformat-signedness -Wnormalized=nfc -Wduplicated-cond -Wnull-dereference -Wshift-negative-value -Wshift-overflow=2 -Wunused-const-variable=2 -Walloca -Walloc-zero -Wformat-overflow=2 -Wformat-truncation=2 -Wstringop-overflow=3 -Wduplicated-branches -Wcast-align=strict -Wsuggest-attribute=cold -Wsuggest-attribute=malloc -Wattribute-alias=2 -Wanalyzer-too-complex -Warith-conversion -Wbad-function-cast -Wmissing-prototypes -Wnested-externs -Wstrict-prototypes -Wold-style-definition -Winit-self -Wc++-compat -Wunsuffixed-float-constants"

GCC="gcc"
CLANG="clang"
if [[ $i == *.cpp ]]; then
  GCC="g++"
  CLANG="clang++"
  WARN_FLAGS="-Wall -Winvalid-pch -Wextra -Wpedantic -Wcast-qual -Wconversion -Wfloat-equal -Wformat=2 -Winline -Wmissing-declarations -Wredundant-decls -Wshadow -Wundef -Wuninitialized -Wwrite-strings -Wdisabled-optimization -Wpacked -Wpadded -Wmultichar -Wswitch-default -Wswitch-enum -Wunused-macros -Wmissing-include-dirs -Wunsafe-loop-optimizations -Wstack-protector -Wstrict-overflow=5 -Warray-bounds=2 -Wlogical-op -Wstrict-aliasing=3 -Wvla -Wdouble-promotion -Wsuggest-attribute=const -Wsuggest-attribute=noreturn -Wsuggest-attribute=pure -Wtrampolines -Wvector-operation-performance -Wsuggest-attribute=format -Wdate-time -Wformat-signedness -Wnormalized=nfc -Wduplicated-cond -Wnull-dereference -Wshift-negative-value -Wshift-overflow=2 -Wunused-const-variable=2 -Walloca -Walloc-zero -Wformat-overflow=2 -Wformat-truncation=2 -Wstringop-overflow=3 -Wduplicated-branches -Wcast-align=strict -Wsuggest-attribute=cold -Wsuggest-attribute=malloc -Wattribute-alias=2 -Wanalyzer-too-complex -Warith-conversion -Winit-self -fno-rtti -fno-exceptions"
fi

$GCC -O3 -ggdb $WARN_FLAGS -fanalyzer -Wno-analyzer-too-complex -o mount-sysroot.gcc $i -lblkid &
$CLANG -O3 -ggdb -Weverything -Wno-c++98-compat -Wno-declaration-after-statement -Wno-unsafe-buffer-usage -o mount-sysroot.clang $i -lblkid &
wait

mv mount-sysroot.gcc mount-sysroot
rm mount-sysroot.clang
valgrind ./mount-sysroot || true
done



