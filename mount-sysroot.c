#include "util-linux-automotive.h"

#include <blkid/blkid.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#define pure_ __attribute__ ((pure))
#define autofclose __attribute__ ((cleanup (cleanup_fclose)))
#define autoclose __attribute__ ((cleanup (cleanup_close)))

__attribute__ ((format (printf, 1, 2))) static int
printe (const char *fmt, ...)
{
  va_list args;
  va_start (args, fmt);
  const int ret = vfprintf (stderr, fmt, args);
  va_end (args);

  return ret;
}

static inline void
cleanup_fclose (FILE **stream)
{
  if (*stream)
    fclose (*stream);
}

static inline void
cleanup_close (int *fd)
{
  if (*fd >= 0)
    close (*fd);
}

static char *
read_proc_cmdline (void)
{
  autofclose FILE *f = fopen ("/proc/cmdline", "r");
  if (!f)
    {
      perror ("Failed to open /proc/cmdline");
      return NULL;
    }

  char *cmdline = NULL;
  size_t len;

  /* Note that /proc/cmdline will not end in a newline, so getline
   * will fail unless we provide a length.
   */
  if (getline (&cmdline, &len, f) < 0)
    {
      perror ("Failed to read /proc/cmdline");
      return NULL;
    }

  /* ... but the length will be the size of the malloc buffer, not
   * strlen().  Fix that.
   */
  len = strlen (cmdline);
  if (cmdline[len - 1] == '\n')
    cmdline[len - 1] = '\0';

  return cmdline;
}

static char *
find_proc_cmdline_key (const char *cmdline, const char *key)
{
  const size_t key_len = strlen (key);
  for (const char *iter = cmdline; iter;)
    {
      const char *next = strchr (iter, ' ');
      if (strncmp (iter, key, key_len) == 0 && iter[key_len] == '=')
        {
          const char *start = iter + key_len + 1;
          if (next)
            {
              UNSAFE_START
              const size_t len = next - start;
              UNSAFE_END
              return strndup (start, len);
            }

          return strdup (start);
        }

      if (next)
        next += strspn (next, " ");

      iter = next;
    }

  return NULL;
}

static bool pure_
has_proc_cmdline_flag (const char *cmdline, const char *key)
{
  const size_t key_len = strlen (key);
  for (const char *iter = cmdline; iter;)
    {
      const char *next = strchr (iter, ' ');
      size_t val_len = 0;

      if (next)
        {
          UNSAFE_START
          const size_t len = next - iter;
          UNSAFE_END
          val_len = len;
        }
      else
        val_len = strlen (iter);

      if (val_len == key_len && strncmp (iter, key, key_len) == 0)
        return true;

      if (next)
        next += strspn (next, " ");

      iter = next;
    }

  return false;
}

static void
print_exists (const char *const fname)
{
  struct stat sb;
  if (stat (fname, &sb))
    {
      perrorf ("%s doesn't exist", fname);
      return;
    }

  perrorf ("%s exists", fname);
}

static int
attempt_mount (const char *const device, const unsigned long sysroot_mount_flags,
               const char *const fstype)
{
  debug ("Mounting /sysroot from %s (%s)", device,
         ((sysroot_mount_flags & MS_RDONLY) != 0) ? "readonly" : "readwrite");
  if (mount (device, "/sysroot", fstype, sysroot_mount_flags, NULL) == -1)
    {
      perrorf ("mount(\"%s\", \"/sysroot\", \"%s\", %lu, NULL)", device, fstype,
               sysroot_mount_flags);
      print_exists (device);
      print_exists ("/sysroot");
      return EXIT_FAILURE;
    }

  debug ("Mount /sysroot success");

  return EXIT_SUCCESS;
}

static char *
remove_outer_quotes (char *str)
{
  size_t len = strlen (str);

  if (len >= 2)
    {
      if (str[len - 1] == '"')
        str[len - 1] = '\0';

      if (str[0] == '"')
        ++str;
    }

  return str;
}

static int
each_block_device (const char *devname, const size_t prefix_size, char *to_compare)
{
  autoblkid_free_probe blkid_probe pr = blkid_new_probe ();
  if (!pr)
    return -1;

  blkid_probe_enable_superblocks (pr, 0);
  blkid_probe_enable_partitions (pr, 1);
  blkid_probe_set_partitions_flags (pr, BLKID_PARTS_ENTRY_DETAILS);
  autoclose int fd = open (devname, O_RDONLY | O_CLOEXEC);
  if (fd < 0)
    return errno;

  if (blkid_probe_set_device (pr, fd, 0, 0))
    return -1;

  const int rc = blkid_do_safeprobe (pr);
  if (rc)
    return rc;

  UNSAFE_START
  char *val;
  if (prefix_size == sizeof ("PARTLABEL")
      && !blkid_probe_lookup_value (pr, "PART_ENTRY_NAME", &val, NULL)
      && !strcmp (val, remove_outer_quotes (to_compare + sizeof ("PARTLABEL"))))
    return 0;

  if (prefix_size == sizeof ("PARTUUID")
      && !blkid_probe_lookup_value (pr, "PART_ENTRY_UUID", &val, NULL)
      && !strcmp (val, remove_outer_quotes (to_compare + sizeof ("PARTUUID"))))
    return 0;
  UNSAFE_END

  return -2;
}

static char *
get_part_device (char *token, const size_t prefix_size)
{
  autoglobfree glob_t glob_result;
  for (int i = 0; i < num_patterns; ++i)
    glob (patterns[i], i ? GLOB_APPEND : 0, NULL, &glob_result);

  for (size_t i = 0; i < glob_result.gl_pathc; ++i)
    if (each_block_device (glob_result.gl_pathv[i], prefix_size, token) == 0)
      return strdup (glob_result.gl_pathv[i]);

  return NULL;
}

static char *
get_device_from_token (char *token, const size_t prefix_size)
{
  if (prefix_size == sizeof ("PARTUUID"))
    return get_part_device (token, sizeof ("PARTUUID"));
  else if (prefix_size == sizeof ("PARTLABEL"))
    return get_part_device (token, sizeof ("PARTLABEL"));

  return blkid_get_devname (NULL, token, NULL);
}

static int
get_device (const char *const cmdline, char **device)
{
  *device = find_proc_cmdline_key (cmdline, "mount-sysroot.root");
  if (*device == NULL)
    *device = find_proc_cmdline_key (cmdline, "root");

  if (*device == NULL)
    {
      perror ("No mount-sysroot.root= or root= karg present in /proc/cmdline");
      return EXIT_FAILURE;
    }

  debug ("Looking up %s", *device);

  size_t prefix_size = 0;
  if (strncmp (*device, "PARTUUID=", sizeof ("PARTUUID")) == 0)
    prefix_size = sizeof ("PARTUUID");
  else if (strncmp (*device, "PARTLABEL=", sizeof ("PARTLABEL")) == 0)
    prefix_size = sizeof ("PARTLABEL");
  else if (strncmp (*device, "UUID=", sizeof ("UUID")) == 0)
    prefix_size = sizeof ("UUID");
  else if (strncmp (*device, "LABEL=", sizeof ("LABEL")) == 0)
    prefix_size = sizeof ("LABEL");

  if (prefix_size)
    {
      char *devname = get_device_from_token (*device, prefix_size);
      if (*device == NULL)
        {
          perror ("Error getting device name");
          return EXIT_FAILURE;
        }

      free (*device);
      *device = devname;
    }

  debug ("Found device %s", *device);

  return EXIT_SUCCESS;
}

static void
msleep (long milliseconds)
{
  struct timespec req, rem;

  // Convert milliseconds to seconds and nanoseconds
  req.tv_sec = milliseconds / 1000;
  req.tv_nsec = (milliseconds % 1000) * 1000000L;
  nanosleep (&req, &rem);
}

// Function to attempt mounting the device with retries
static int
mount_device_with_retries (const char *cmdline, unsigned long sysroot_mount_flags,
                           const char *fstype)
{
  int ret = 0;
  for (long i = 1; i < 4000; i *= 2)
    {
      autofree char *device = NULL;
      ret = get_device (cmdline, &device);
      if (ret)
        {
          msleep (i);
          printe ("Retrying get_device(\"%s\", \"%s\")\n", cmdline, device ? device : "(null)");
          continue;
        }

      ret = attempt_mount (device, sysroot_mount_flags, fstype);
      if (ret)
        {
          msleep (i);
          printe ("Retrying attempt_mount(\"%s\", %lu, \"%s\")\n", device, sysroot_mount_flags,
                  fstype);
          continue;
        }

      break;
    }

  return ret;
}

int
main (void)
{
  debug ("Starting");

  const autofree char *cmdline = read_proc_cmdline ();
  if (cmdline == NULL)
    return EXIT_FAILURE;

  enable_debug = has_proc_cmdline_flag (cmdline, "mount-sysroot.debug");
  autofree char *fstype = find_proc_cmdline_key (cmdline, "mount-sysroot.rootfstype");
  if (fstype == NULL)
    fstype = find_proc_cmdline_key (fstype, "rootfstype");

  unsigned long sysroot_mount_flags = 0;
  if (has_proc_cmdline_flag (cmdline, "mount-sysroot.ro")
      || (!has_proc_cmdline_flag (cmdline, "mount-sysroot.rw")
          && has_proc_cmdline_flag (cmdline, "ro")))
    sysroot_mount_flags |= MS_RDONLY;

  return mount_device_with_retries (cmdline, sysroot_mount_flags, fstype ? fstype : "ext4");
}
