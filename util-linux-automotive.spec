Name:          util-linux-automotive
Version:       0.4.1
Release:       1%{?dist}
Summary:       A random collection of Linux utilities for automotive
License:       GPL-2.0-only
URL:           https://gitlab.com/CentOS/automotive/rpms/util-linux-automotive
Source0:       mount-sysroot.c
Source1:       dev-disk-populate.cpp
Source2:       dev-disk-populate.service
Source3:       util-linux-automotive.h

BuildRequires: systemd
BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: libblkid-devel
Requires: libblkid

%global debug_package %{nil}

%description
%{summary}.

%prep

%build
RPM_OPT_FLAGS="${RPM_OPT_FLAGS/-flto=auto /}"
gcc ${RPM_OPT_FLAGS} -lblkid %{SOURCE0} -o mount-sysroot
g++ ${RPM_OPT_FLAGS} -lblkid %{SOURCE1} -fno-rtti -fno-exceptions -o dev-disk-populate

%install
install -D -m755 mount-sysroot ${RPM_BUILD_ROOT}/%{_bindir}/mount-sysroot
install -D -m755 dev-disk-populate ${RPM_BUILD_ROOT}/%{_bindir}/dev-disk-populate
install -D -m644 %{SOURCE2} ${RPM_BUILD_ROOT}/%{_unitdir}/dev-disk-populate.service

%files
%{_bindir}/mount-sysroot
%{_bindir}/dev-disk-populate
%{_unitdir}/dev-disk-populate.service

%changelog
* Tue Jan 21 2025 Brian Masney <bmasney@redhat.com> - 0.4.1-1
- Add more debug logging, add a .gitignore, and set build.sh to executable.
* Fri Jan 6 2025 Eric Curtin <ecurtin@redhat.com> - 0.4-1
- Introduce retries with exponential backoff
* Fri Jun 14 2024 Eric Curtin <ecurtin@redhat.com> - 0.3-1
- Introduce gpt parsing
* Fri Jun 7 2024 Eric Curtin <ecurtin@redhat.com> - 0.2-1
- Introduce dev-disk-populate
* Thu May 23 2024 Eric Curtin <ecurtin@redhat.com> - 0.1-6
- Mount ext4 by default
* Thu May 23 2024 Eric Curtin <ecurtin@redhat.com> - 0.1-5
- Be more verbose with logging
* Thu May 23 2024 Eric Curtin <ecurtin@redhat.com> - 0.1-4
- Check for existance of files on failure
* Thu May 23 2024 Eric Curtin <ecurtin@redhat.com> - 0.1-3
- Add perrorf
* Wed May 22 2024 Eric Curtin <ecurtin@redhat.com> - 0.1-2
- Better debug
* Wed May 22 2024 Eric Curtin <ecurtin@redhat.com> - 0.1-1
- Initial version

